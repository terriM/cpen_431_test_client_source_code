Run the tests with the following command:
java -jar a3_tests.jar <path-to-`server.list`>

Where server.list contains a single line of the form ipAddress:port#:maxTime

Where maxTime is the amount of time to run the out of memory test in minutes. The out of
memory test sends randomly generated put requests as fast as possible to attempt to 
make the server run out of memory.

Note that tests distinguish between a network timeout failure
and server failure.

***Tests***

Basic tests include:
* ping server to make sure it is alive
* get pid request

Erroneous message tests include:
* sends key that is too long
* sends value that is too long
* sends put request with no key or value
* send put request with no value
* sends get request with no key

Packet corruption includes:
* send invalid checksum

At most once includes:
* put / get / remove / get where second get has same ID as first. Returned results should be PUT value assuming network is fast enough

Out of memory test includes: (pending - needs to be verified)
* repeat the following multiple times to break the server in different places
* send put requests of random keys and values as fast as possible until out of memory response is received. Send a get request right away and make sure it passes
*  once full, wait amount of time for cache to empty, then retry put which should be successful
* send deleteAll and repeat this test multiple times so the server will send out of memory at different points in the application

Shutdown program includes: 
* receive success message on shutdown command
* send isalive ping, should not return