package com.s45578127.CPEN431.A3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import com.google.protobuf.ByteString;
import com.s45578127.CPEN431.A3.KeyValueRequest.KVRequest;
import com.s45578127.CPEN431.A3.KeyValueResponse.KVResponse;

public class LayerApplication {

	private final static int maxMessageSize = 16000;  // bytes
	
	private static LayerTransport ltrans;
	
	public static void main(String[] args){
		/**
		 * Initialize log file
		 */
		PrintStream log;
		
		try {
			log = new PrintStream(new FileOutputStream("A3_Adv.log"));
		} catch (FileNotFoundException e1) {
			System.out.println("log file could not be created.");
			System.exit(1);
			return;
		}
		
		//System.setOut(log);

		/**
		 * Parse information from argument file
		 */
		
		if(args.length != 1){
			System.out.println("Invalid number of command line entries. Must include server.log file");
			System.exit(1);
		}
		
		File file;
		file = new File(args[0]);
		
		Scanner scan;
		try {
			scan = new Scanner(file).useDelimiter(":");
		} catch (FileNotFoundException e) {
			System.out.println("Invalid command line entry. Must include server.log file");
			System.exit(1);
			return;
		}
		
		InetAddress serverIp;
		try {
			serverIp = InetAddress.getByName(scan.next());
		} catch (UnknownHostException | NoSuchElementException e) {
			System.out.println("Invalid IP address.");
			System.exit(1);
			return;
		}
		
		int port;
		try{
			port = Integer.parseInt(scan.next().trim());
		} catch(NumberFormatException | NoSuchElementException e) {
			System.out.println("Server port number is invalid");
			System.exit(1);
			return;
		}
		
		if(port < 0 || port > 65535){
			System.out.println("Server port number is invalid");
			System.exit(1);
			return;
		}
		
		int maxTime; // time to run out of memory in minutes
		try{
			maxTime = Integer.parseInt(scan.next().trim());
		} catch(NumberFormatException | NoSuchElementException e) {
			System.out.println("Must include time in minutes for out of memory test");
			System.exit(1);
			return;
		}
		
		scan.close();
		
		/**
		 * Create instance of transport file to communicate
		 * with server
		 */
		InetAddress clientIp;
		try {
			clientIp = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			System.out.println("Could not get local IP address");
			System.exit(1);
			return;
		}
		ltrans = new LayerTransport(serverIp, port, clientIp, maxMessageSize);
		
		/**
		 * Run test suite
		 */
		System.out.println("[A3 Advanced Tests]");
		
		runBasicTest();
		runErroneousMessageTest();
		runPacketCorruptionTest();
		runAtMostOnceTest();
		runOutOfMemoryTest(maxTime);
		runShutdownTest();
	}

	/**
	 * Prints test name in uniform format
	 * @param name - name of test to be run
	 */
	private static void testBegin(String name) {
		System.out.println("\n[TEST " + name + "]");
	}
	
	/**
	 * Prints result of test to log file
	 * @param result - true is test passed, false if failed
	 */
	private static void testResult(Boolean result) {
		if(result) System.out.println("TEST_PASSED");
		else {
			System.out.println("TEST_FAILED");
			System.exit(0);
		}
	}
	
	/**
	 * Basic tests include:
	 * - ping server to make sure it is alive
	 * - get pid request
	 */
	private static void runBasicTest() {
		KVResponse resp;
		
		/**
		 * IsAlive test
		 */
		testBegin("isAlive]");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.ISALIVE));
		try{
			testResult(resp.getErrCode() == RepCode.SUCCESS.getCodeVal());
		} catch(NullPointerException e) { testResult(false); return; }
		
		/**
		 * Get PID test
		 */
		testBegin("getPID");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.GETPID));
		try{
			Boolean result = (resp.getErrCode() == RepCode.SUCCESS.getCodeVal());
			if(result) System.out.println("PID is: " + resp.getPid());
			testResult(result);
		} catch(NullPointerException e) { testResult(false); }
		
	}
	
	/**
	 * Sends erroneous put request messages to server:
	 * - sends key that is too long
	 * - sends key that is too short
	 * - sends value that is too long
	 * - sends put request with no key or value
	 * - send put request with no value
	 * - sends get request with no key
	 */
	private static void runErroneousMessageTest() {
		KVResponse resp;
		
		ByteString keyValid = ByteString.copyFromUtf8(new String(new char[16]).replace('\0', '0'));
		ByteString valueValid = ByteString.copyFromUtf8(new String(new char[50]).replace('\0', '0'));
		
		/**
		 * Send key that is too long
		 */
		ByteString keyLong = ByteString.copyFromUtf8(new String(new char[33]).replace('\0', '0'));
		
		testBegin("Send invalid key: too long");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.PUT, keyLong, valueValid));
		try{
			testResult(resp.getErrCode() == RepCode.INVKEY.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
		/**
		 * Send value that is too long
		 */
		ByteString valueLong = ByteString.copyFromUtf8(new String(new char[11000]).replace('\0', '0'));
		
		testBegin("Send invalid value: too long");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.PUT, keyValid, valueLong));
		try{
			testResult(resp.getErrCode() == RepCode.INVVAL.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
		/**
		 * Send put request with no key or value
		 */	
		testBegin("Send invalid put request with no key / value");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.PUT));
		try{
			testResult(resp.getErrCode() == RepCode.INVKEY.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
		/**
		 * Send put request with no value
		 */
		testBegin("Send invalid put request with no value");
		resp = ltrans.sendThroughTransport(KVRequest.newBuilder()
				.setCommand(ReqCmd.PUT.getCmdID())
				.setKey(keyValid)
				.build());
		try{
			testResult(resp.getErrCode() == RepCode.INVVAL.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
		/**
		 * Send get request with no key
		 */
		testBegin("Send invalid get request with no key");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.GET));
		try{
			testResult(resp.getErrCode() == RepCode.INVKEY.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
	}
	
	/**
	 * Packet corruption includes:
	 * - send invalid checksum
	 */
	private static void runPacketCorruptionTest() {
		testBegin("Packet Corruption");
		
		KVRequest req1 = createRequestMessage(ReqCmd.ISALIVE);
		KVRequest req2 = createRequestMessage(ReqCmd.GETPID);
		
		// test needs to be done at the transport layer
		Boolean result = ltrans.runTransportPacketCorruptionTest(req1, req2);
		testResult(result);
		
	}
	
	/**
	 * At most once includes:
	 * - put / get / remove / get where second get has same ID as first
	 * 		returned result should be the PUT value (tests performance
	 * 		also somewhat - assumes message will be processed quickly
	 * 		enough so the second get will still be in the cache)
	 */
	private static void runAtMostOnceTest() {
		testBegin("At Most Once");
		
		ByteString key = ByteString.copyFromUtf8(new String(new char[16]).replace('\0', '0'));
		ByteString value = ByteString.copyFromUtf8(new String(new char[50]).replace('\0', '0'));
		
		// create requests
		KVRequest putReq = createRequestMessage(ReqCmd.PUT, key, value);
		KVRequest getReq = createRequestMessage(ReqCmd.GET, key);
		KVRequest remReq = createRequestMessage(ReqCmd.REMOVE, key);
		
		// test needs to be done at the transport layer
		Boolean result = ltrans.runTransportAtMostOnceTest(putReq, getReq, remReq);
		testResult(result);
	}
	
	/**
	 * Run out of memory test multiple times.
	 * That way randomly generated keys and values
	 * will break system in multiple places.
	 * @args - maximum number of minutes to run out of memory 
	 * before timeout
	 */
	private static void runOutOfMemoryTest(int maxTime) {
		
		testBegin("Out of Memory Series");
		
		testBegin("Out of Memory");
		runOutOfMemorySubTest(maxTime);
	}
	
	/** Out of memory tests include:
	 *  - send put requests of random keys and values as fast as possible
	 *  until out of memory response is received. Send a get request right
	 *  away and make sure it passes
	 *  - once full, wait amount of time for cache to empty, then retry put
	 *  which should be successful
	 *  - send deleteAll and repeat this test multiple times so the server
	 *  will send out of memory at different points in the application
	 * @args - maximum number of minutes to run out of memory 
	 * before timeout
	 */
	private static void runOutOfMemorySubTest(int maxTime) {
		KVResponse resp;
		Random rand = new Random();
		
		/**
		 * Send put requests until out of memory response is received
		 */
		int nKey, nVal;
		ByteString key = null, value = null;
		
		long endTime = System.currentTimeMillis() + maxTime*60*1000;
		Boolean isOutOfMemory = false;
		
		while(System.currentTimeMillis() < endTime){
			
			// generate random keys and values
			// 32 is the maximum and the 1 is our minimum 
			nKey = rand.nextInt(32) + 1;
			if(nKey < 20) nKey = 20;
			// 10000 is the maximum and the 1 is our minimum 
			nVal = rand.nextInt(10000) + 1;
			if(nVal < 9000) nVal = 9000;
			key = ByteString.copyFromUtf8(new String(new char[nKey]).replace('\0', '0'));
			value = ByteString.copyFromUtf8(new String(new char[nVal]).replace('\0', '0'));
			
			resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.PUT, key, value));
			
			if(resp == null){
				System.out.println("Did not receive response from put request");
				testResult(false);
				return;
			} else if(resp.getErrCode() == RepCode.SUCCESS.getCodeVal()) {
				continue;
			} else if (resp.getErrCode() == RepCode.OOSPACE.getCodeVal()) {
				isOutOfMemory = true;
				break;
			}
		}
		
		if(isOutOfMemory){
			/**
			 * Send get request right away to make sure it works when the 
			 * system is out of memory.
			 */
			resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.GET, key, value));
			try{
				testResult(resp.getErrCode() != RepCode.OOSPACE.getCodeVal());
			} catch (NullPointerException e){ testResult(false); } 
			
			/**
			 * Wait for cache to empty, then resend put.
			 */
			try {
				TimeUnit.SECONDS.sleep(10);
			} catch (InterruptedException e1) {
				System.out.println("Sleep failed..");
				testResult(false);
				return; 
			}
			
			resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.PUT, key, value));
			try{
				testResult(resp.getErrCode() == RepCode.SUCCESS.getCodeVal());
			} catch (NullPointerException e){ testResult(false); } 
		} else {
			System.out.println("Client reached specified time for out of memory test without reaching end of memory");
			testResult(true);
		}
		
		/**
		 * clear key-value
		 */
		ltrans.sendThroughTransport(createRequestMessage(ReqCmd.DELETEALL));
		
		
	}
	
	/**
	 * Shutdown program
	 * - receive success message on shutdown command
	 * - send isalive ping, should not return
	 */
	private static void runShutdownTest() {
		KVResponse resp;
		
		/**
		 * Send shutdown request
		 */
		testBegin("shutdown");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.SHUTDOWN));
		try{
			testResult(resp.getErrCode() == RepCode.SUCCESS.getCodeVal());
		} catch (NullPointerException e) { testResult(false); }
		
		/**
		 * Send isalive request. Should not return anything.
		 */
		testBegin("isalive: shutdown check");
		resp = ltrans.sendThroughTransport(createRequestMessage(ReqCmd.ISALIVE));
		testResult(resp == null);
		
	}

	/**
	 * Create a server request message.
	 * @param cmd - command request to send to server
	 * @return built request message in byte string format
	 */
	private static KVRequest createRequestMessage(ReqCmd cmd){
		return KVRequest.newBuilder()
				.setCommand(cmd.getCmdID())
				.build();
	}

	/**
	 * Create an application request message
	 * @param cmd - command request to send to server
	 * @param value - value from key-value struct to send to server
	 * @return built request message
	 */
	private static KVRequest createRequestMessage(ReqCmd cmd, ByteString key){
		return KVRequest.newBuilder()
				.setCommand(cmd.getCmdID())
				.setKey(key)
				.build();
	}
	
	/**
	 * Create an application request message
	 * @param cmd - command request to send to server
	 * @param value - value for key-value struct to send to server
	 * @param key - key for key-value struct to send to server
	 * @return built request message
	 */
	private static KVRequest createRequestMessage(ReqCmd cmd, ByteString key, ByteString value){
		return KVRequest.newBuilder()
				.setCommand(cmd.getCmdID())
				.setKey(key)
				.setValue(value)
				.build();
	}
}
