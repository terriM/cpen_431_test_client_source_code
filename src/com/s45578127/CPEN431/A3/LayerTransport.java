package com.s45578127.CPEN431.A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.s45578127.CPEN431.A3.KeyValueRequest.KVRequest;
import com.s45578127.CPEN431.A3.KeyValueResponse.KVResponse;
import com.s45578127.CPEN431.A3.Message.Msg;

/**
 * This file contains code copied from my assignment 2 for the
 * server transport layer.
 * @author theresamammarella
 *
 */

public class LayerTransport {

	// default timeout in milliseconds
	private final static int defaultTimeout = 100;
	private final static int numMessageRetries = 3;
	
	// maximum payload size is 16kB
	private final static int uniqueIdSize = 16;
	
	// instance variables
	private InetAddress serverIp;
	private int port;
	private InetAddress clientIp;
	private int payloadSize;
	
	public LayerTransport(InetAddress serverIp, int port, InetAddress clientIp, int maxPayloadSize){
		this.serverIp = serverIp;
		this.port = port;
		this.clientIp = clientIp;
		this.payloadSize = maxPayloadSize;
	}

	/**
	 * Getter method for default application payload size
	 * @return payload size in bytes
	 */
	public int getPayloadSize(){
		return payloadSize;
	}
	
	/**
	 * Getter method for size of unique message id
	 * @return unique id size in bytes
	 */
	public static int getUniqueIdSize(){
		return uniqueIdSize;
	}
	
	/**
	 * Getter method for size of full request message
	 * @return total message size in bytes
	 */
	public int getRequestMessageSize(){
		return (uniqueIdSize + payloadSize) * 2; // two times for max checksum size
	}
	
	/**
	 * Transport layer function prepares UDP message to send to server, sends 
	 * the message and waits to receive a reply
	 * @param requestPayload - application payload to send to server
	 * @return - response paylod message if receive was successful, else null
	 */
	public KVResponse sendThroughTransport(KVRequest requestPayload) {
		
		/**
		 *  prepare request message to send to server
		 */
		Msg requestMsg = createRequestMessage(requestPayload);
		
		return sendUdpRequest(requestMsg);
	}
	
	/**
	 * Transport layer function sends  UDP
	 * message and waits to receive a reply
	 * @param requestPayload - application payload to send to server
	 * @return - response paylod message if receive was successful, else null
	 */
	private KVResponse sendUdpRequest(Msg requestMsg){
		/**
		 *  attempt to receive from server
		 */
		// set starting timeout
		int timeout = defaultTimeout;
		
		// client binds to specified port number
		DatagramSocket socket;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e1) {
			return null;
		}
		
		// initialize output packet
		byte[] requestBuffer = requestMsg.toByteArray(); // serialize message
		DatagramPacket requestPacket = new DatagramPacket(requestBuffer, requestBuffer.length, serverIp, port);
		
		// initialize input packet
		byte[] responseBuffer = new byte[getRequestMessageSize()];
		DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length);
		
		for(int i = 0; i <= numMessageRetries; i++){
			/**
			 *  attempt to send message to server
			 */
			try {
				socket.send(requestPacket);
			} catch (IOException e) {
				e.printStackTrace();
				socket.close();
				return null; // communication with server failed
			}
			
			// set timeout for next try
			try {
				socket.setSoTimeout(timeout);
			} catch (SocketException e1) {
				socket.close();
				return null;
			}
			// double timeout for next retry
			timeout *= 2;

			/**
			 * attempt to receive message from server
			 */
			try {
				socket.receive(responsePacket);
			} catch (SocketTimeoutException e){
				continue; // receive timed out, reattempt send-receive
			}catch (IOException e) {
				e.printStackTrace();
				socket.close();
				return null; // communication with server failed
			}

			/**
			 * Verify received message
			 */
			// delimited message buffer must be exact length of message
			byte[] responseMessageBuffer = Arrays.copyOfRange(responseBuffer, 0, responsePacket.getLength());
			
			Msg responseMsg;
			try {
				responseMsg = Msg.parseFrom(responseMessageBuffer);
			} catch (IOException e) {
				e.printStackTrace();
				socket.close();
				return null;
			}
			
			// verify that checksum has not been corrupted
			long calculatedCheckSum = createMessageCheckSum(responseMsg);
			if(calculatedCheckSum != responseMsg.getCheckSum()){
				System.out.println("Checksum is invalid");
				continue; // packet is corrupt
			}
			
			// verify that unique ID matches sent message
			if(requestMsg.getMessageID().equals(responseMsg.getMessageID()) == false){
				System.out.println("Unique ID is invalid");
				continue; // IDs do not match, reattempt send-receive
			}
			
			// client received correct message, return received array to application layer
			socket.close();
			
			KVResponse responsePayload;
			try {
				responsePayload = KVResponse.parseFrom(responseMsg.getPayload());
			} catch (InvalidProtocolBufferException e) {
				return null;
			}
			return responsePayload;
		}

		// if code gets to here, message was not received after all retries
		System.out.println("Message timed out: Network Error");
		socket.close();
		return null;
	}
	
	/**
	 * Create message to send to server with application payload
	 * @param clientAddress - client IP address
	 * @param port - port to communicate with server
	 * @param requestPayload - application payload to send to server
	 * @return - built Msg to send to server
	 */
	private Msg createRequestMessage(KVRequest requestPayload) {
		Msg.Builder builder = Msg.newBuilder();
		
		builder.setMessageID(createMessageID());
		builder.setPayload(requestPayload.toByteString());
		builder.setCheckSum(createMessageCheckSum(builder.buildPartial()));
		
		return builder.build();
	}

	/**
	 * Creates checksum for message and returns value as long
	 * @param requestPayload 
	 * @param requestMsg
	 * @return message checksum value
	 */
	private static long createMessageCheckSum(Msg message) {
		byte[] messageIDByte = message.getMessageID().toByteArray();
		byte[] payloadByte = message.getPayload().toByteArray();
		
		Checksum checksum = new CRC32();
		
		// CRC32(messageID +++ payload) 
		byte[] checkSumByte = new byte[messageIDByte.length + payloadByte.length];
		
		// add message id
		int startIndex = 0;
		System.arraycopy(messageIDByte, 0, checkSumByte, startIndex, messageIDByte.length);
		
		// add payload
		startIndex = messageIDByte.length;
		System.arraycopy(payloadByte, 0, checkSumByte, startIndex, payloadByte.length);
		
		checksum.update(checkSumByte, 0, checkSumByte.length);
		
		return checksum.getValue();
	}

	/**
	 * Creates unique ID for request message and sets global variable
	 * @param clientAddress - client IP address
	 * @param port - port number to contact server
	 * @return ByteString - return unique ID in byte string form
	 */
	private ByteString createMessageID(){
		byte[] messageUniqueID = new byte[getUniqueIdSize()];
		ByteBuffer byteBuff = ByteBuffer.wrap(messageUniqueID);
		
		// first four bytes are client IP
		byteBuff.put(clientIp.getAddress());
		
		// next two bytes are port number
		// short is two bytes while int is 4
		byteBuff.putShort((short)port);
		
		// next 2 bytes randomly generated
		byte[] randomBytes = new byte[2];
		new Random().nextBytes(randomBytes);
		byteBuff.put(randomBytes);
		
		// next 8 bytes are time stamp
		byteBuff.putLong(System.currentTimeMillis());
		
		// return unique ID as ByteString
		return ByteString.copyFrom(messageUniqueID);
	}
	
	/**********************************************************
	 ***            Server Transport Layer Tests            ***
	 **********************************************************/

	/**
	 * At most once includes:
	 * - put / get / remove / get where second get has same ID as first
	 * 		returned result should be the PUT value (tests performance
	 * 		also somewhat - assumes message will be processed quickly
	 * 		enough so the second get will still be in the cache)
	 * @return - true if test passed, false if failed
	 */
	public Boolean runTransportAtMostOnceTest(KVRequest putReq, KVRequest getReq, KVRequest remReq) {
		KVResponse resp;
		
		Msg putMsg = createRequestMessage(putReq);
		Msg getMsg = createRequestMessage(getReq);
		Msg remMsg = createRequestMessage(remReq);
		
		/**
		 * send put / get / remove with success
		 */
		resp = sendUdpRequest(putMsg);
		try{
			if(resp.getErrCode() != RepCode.SUCCESS.getCodeVal()) return false;
		}catch (NullPointerException e){ return false; }
		
		resp = sendUdpRequest(getMsg);
		try{
			if(resp.getErrCode() != RepCode.SUCCESS.getCodeVal()) return false;
		}catch (NullPointerException e){ return false; }
		
		resp = sendUdpRequest(remMsg);
		try{
			if(resp.getErrCode() != RepCode.SUCCESS.getCodeVal()) return false;
		}catch (NullPointerException e){ return false; }
	
		/**
		 * Send the same get request immediately after, expect success
		 */
		resp = sendUdpRequest(getMsg);
		try{
			return (resp.getErrCode() == RepCode.SUCCESS.getCodeVal());
		}catch (NullPointerException e){ return false; }
	}

	/**
	 * Packet corruption includes:
	 * - send invalid checksum
	 */
	public Boolean runTransportPacketCorruptionTest(KVRequest reqReal, KVRequest reqFaux) {
		Msg msgReal = createRequestMessage(reqReal);
		
		// build message with incorrect checksum
		Msg msgFaux = Msg.newBuilder()
			.setMessageID(createMessageID())
			.setPayload(reqFaux.toByteString())
			.setCheckSum(msgReal.getCheckSum())
			.build();
		
		// send message with incorrect checksum to server
		KVResponse resp = sendUdpRequest(msgFaux);
		
		return(resp == null);
	}
}
