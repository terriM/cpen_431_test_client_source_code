package com.s45578127.CPEN431.A3;

public enum RepCode {
	SUCCESS(0x00), // Operation is successful.
	NOKEY(0x01), // Non-existent key requested in a get or delete operation 
	OOSPACE(0x02), // Out of space  (returned when there is no space left for a put).
	SYSOVERLOAD(0x03), // System overload.  The system decides to refuse the operation due to temporary overload
	KVFAIL(0x04), // Internal KVStore failure 
	UNRECCMD(0x05), // Unrecognized command. 
	INVKEY(0x06), // Invalid key:  the key length does not match the expected length. 
	INVVAL(0x07); // Invalid value length:  the value length does not match the expected length. 

	private int codeVal;
	
	RepCode(int codeVal){
		this.codeVal = codeVal;
	}
	
	/**
	 * Returns code value based on specified enum
	 * @return code value
	 */
	int getCodeVal(){
		return this.codeVal;
	}
	
}
