package com.s45578127.CPEN431.A3;

public enum ReqCmd {
	INVALID(0x00), // this value is returned if code is invalid
	PUT(0x01), // Put: This is a put operation.  
	GET(0x02), // Get: This is a get operation. 
	REMOVE(0x03), //Remove: This is a remove operation.
	SHUTDOWN(0x04), //Shutdown: shuts-down the (used for testing and management) 
	DELETEALL(0x05), // DeleteAll: deletes all keys stored in the node (used for testing) 
	ISALIVE(0x06), // IsAlive: does nothing but replies with success if the node is alive.
	GETPID(0x07); // GetPID: the node is expected to reply with the processID of the Java process
	
	private int commandID;
	
	ReqCmd(int commandID){
		this.commandID = commandID;
	}
	
	/**
	 * Returns command id based on enum value
	 * @return command id
	 */
	int getCmdID(){
		return this.commandID;
	}
	
	/**
	 * Finds enum value matching command id
	 * @param command - request command from request message
	 * @return - enum value matching command ID
	 */
	static ReqCmd decodeCommand(int command){
		if(command == PUT.getCmdID()) return PUT;
		if(command == GET.getCmdID()) return GET;
		if(command == REMOVE.getCmdID()) return REMOVE;
		if(command == SHUTDOWN.getCmdID()) return SHUTDOWN;
		if(command == DELETEALL.getCmdID()) return DELETEALL;
		if(command == ISALIVE.getCmdID()) return ISALIVE;
		if(command == GETPID.getCmdID()) return GETPID;
		
		// if function has not yet returned, command is invalid
		return INVALID;
	}
}
